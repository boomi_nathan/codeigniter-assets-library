<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Assets_lib {

    protected $_css, $_js, $_remote_css, $_remote_js = array();
    protected $_config;

    const CSS_EXT = '.css';
    const JS_EXT = '.js';
    const PATH = '/';

    public function __construct() {
        log_message('info', 'Assets Library Class Initialized');
        $this->load->config('assets_lib_config', true);
        $this->_config = $this->config->item('assets_lib_config');
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    public function add_css($css) {
        $this->_setup_assets($css, __FUNCTION__);
    }

    public function add_js($js) {
        $this->_setup_assets($js, __FUNCTION__);
    }

    public function add_remote_css($remote_css) {
        $this->_setup_assets($remote_css, __FUNCTION__);
    }

    public function add_remote_js($remote_js) {
        $this->_setup_assets($remote_js, __FUNCTION__);
    }

    public function get_css() {
        $output = null;

        if ($this->_css) {
            foreach ($this->_css as $value) {
                $file = $this->config->base_url($this->_config['assets_folder'] . self::PATH . $this->_config['css_folder'] . self::PATH . $value . self::CSS_EXT);
                if (file_exists($file)) {
                    $output .= "<link href='" . $file . "' rel='stylesheet'>" . PHP_EOL;
                } else {
                    log_message('debug', 'css file "' . $file . '" does not exist');
                }
            }
        }

        if ($this->_remote_css) {
            foreach ($this->_remote_css as $value) {
                $output .= "<link href='" . $value . "' rel='stylesheet'>" . PHP_EOL;
            }
        }

        echo $output;
    }

    public function get_js() {
        $output = null;

        if ($this->_js) {
            foreach ($this->_js as $value) {
                $file = $this->config->base_url($this->_config['assets_folder'] . self::PATH . $this->_config['js_folder'] . self::PATH . $value . self::JS_EXT);
                if (file_exists($file)) {
                    $output .= "<script src='" . $file . "'/></script>" . PHP_EOL;
                } else {
                    log_message('debug', 'js file "' . $file . '" does not exist');
                }
            }
        }

        if ($this->_remote_js) {
            foreach ($this->_remote_js as $value) {
                $output .= "<script src='" . $value . "'/></script>" . PHP_EOL;
            }
        }

        echo $output;
    }

    protected function _setup_assets($params, $type) {
        $merge = array();

        if (is_array($params)) {
            foreach ($params as $value) {
                $merge[] = $value;
            }
        } else {
            $merge[] = $params;
        }

        switch ($type) {
            case 'add_css':
                $this->_css = array_merge((array) $this->_css, (array) $merge);
                break;
            case 'add_js':
                $this->_js = array_merge((array) $this->_js, (array) $merge);
                break;
            case 'add_remote_css':
                $this->_remote_css = array_merge((array) $this->_remote_css, (array) $merge);
                break;
            case 'add_remote_js':
                $this->_remote_js = array_merge((array) $this->_remote_js, (array) $merge);
                break;
        }
    }

}
