<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config = array(
    'assets_folder' => 'assets',
    'css_folder' => 'css',
    'js_folder' => 'js',
);
