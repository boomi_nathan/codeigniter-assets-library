### Create folders ###
Create folders in the root **(not in application folder)**

-assets

--css

--js

### Load library ###
```
#!php
$this->load->library('assets_lib');
```

## Adding assets ##

```
#!php

$this->assets_lib->add_css($file);
$this->assets_lib->add_remote_css($file);
$this->assets_lib->add_js($file);
$this->assets_lib->add_remote_js($file);

```

**$file** can be pass as array


```
#!php
$this->assets_lib->add_css(array('ddd','www'));
$this->assets_lib->add_remote_css(array('ddd','www'));
$this->assets_lib->add_js(array('ddd','www'));
$this->assets_lib->add_remote_js(array('ddd','www'));

```

**Don't write any extentions of files, like script.js or style.css**

**when add remote assets, write full url, like http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js**

### Assets output ###

```
#!php
$this->assets_lib->get_css();
$this->assets_lib->get_js();
```